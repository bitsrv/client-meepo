package client.meepo

import groovy.json.JsonSlurper
import groovy.util.logging.Log
import wslite.rest.RESTClient

/**
 *
 */
@Log
class MeepoClient {

    String serviceUrl = 'http://pan.bit.edu.cn/v1'
    private RESTClient restClient

    private String token
    private String userId

    MeepoClient(String url = null) {
        if (url != null) { serviceUrl = url}

        restClient = new RESTClient(serviceUrl)
        log.info("Meepo client created")
    }

    private post(path, params, more = [:]) {
        try {
            def resp = restClient.post(path: path, query: params) {
                type 'application/json'
                more.each { k, v ->
                    "${k}" v
                }
            }

            if (resp.response.statusCode == 200) {
                JsonSlurper jsonSlurper = new JsonSlurper()
                def json
                try {
                    json = jsonSlurper.parseText(new String(resp.data, 'UTF-8'))
                } catch (e) {
                }

                log.info("POST ${path}: ${json}。")
                return json
            }
        } catch (e) {
            log.warning("${path}: ${e.message}")
            throw e
        }
    }

    private get(path, params) {
        try {
            def resp = restClient.get(path: path, query: params) {
                type 'application/json'
            }

            if (resp.response.statusCode == 200) {
                JsonSlurper jsonSlurper = new JsonSlurper()
                def json
                try {
                    json = jsonSlurper.parseText(new String(resp.data, 'UTF-8'))
                } catch (e) {
                }

                log.info("GET ${path}: ${json}。")
                return json
            }
        } catch (e) {
            throw e
        }
    }

    def signIn(username, password, linkName, linkDeviceName) {
        def params = [username: username, password: password, link_name: linkName, link_device: linkDeviceName]
        def json = post('/auth/sign_in', params)
        token = json.token ? json.token : null
        userId = json.user_id
//        tokenMap['expires_at'] = link.expires_at?.millis?link.expires_at?.millis:null
    }

    def listUserGroups() {
        def params = [token: token]
        get("/users/${userId}/groups", params)
    }

    def putFileByPath(File file, String rootId, String path, boolean overwrite = false) {
        def params = [token: token, modified_at_millis: file.lastModified(), overwrite: overwrite.toString()]
        post("/roots/${rootId}/files/p${path}", params, [bytes: file.bytes])
    }

    def createFolder(String rootId, String path, long lastModified = System.currentTimeMillis()) {
        def params = [token: token, root_id: rootId, path: path, modified_at_millis: lastModified]
        post("/fileops/create_folder", params)
    }

    Boolean isTokenValid() {
        return !(token == null)
    }

}