import client.meepo.*

class UploadDir {

    def options
    String rootId
    def client

    UploadDir(options) {
        this.options = options
    }

    def start() {
        client = new MeepoClient()
        client.signIn(options.username, options.password, 'upload', 'upload')

        if (options.group) {
            def groups = client.listUserGroups()
            groups['entries'].each {
                if (it.name == options.group) rootId = it.root_id
            }
        }

        println "use root_id: " + rootId

        uploadDir(new File('.'), 0, '')
    }

    def uploadDir(File path, int depth, String prePath) {
        String fileName = path.name == '.' ? '' : prePath + '/' + path.name

        if (path.isDirectory()) {
            if (fileName) {
                if (options.v) {
                    println "create folder: ${fileName}"
                }
                if (!options.dry) {
                    client.createFolder(rootId, fileName)
                }
            }

            File[] files = path.listFiles()
            int i = 1
            for (File file: files) {
                uploadDir(file, depth+1, fileName)
                i ++
            }
        } else {
            if (options.v) {
                println "${path} => ${prePath}/${path.name}"
            }

            if (!options.dry) {
                client.putFileByPath(path, rootId, fileName, true)
            }
        }
    }

    static void main(args) {
        def cli = new CliBuilder(usage:'upload_dir')
        cli.help('print this message')
        cli.group(args: 1, argName: 'group', 'group name')
        cli.username(args: 1, argName:'username', 'user login')
        cli.password(args: 1, argName:'password', 'user password')
        cli.dry('dry run')
        cli.v('verbose')

        def options = cli.parse(args)
        assert options // would be null (false) on failure

        if (options.help) {
            cli.usage()
            return
        }

        new UploadDir(options).start()
    }
}

UploadDir.main(args)

